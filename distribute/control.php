<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<?php include("site.php") ?>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Re-Volt I/O</title>

<style type="text/css">
.section {
  margin-top: 40px;
  margin-bottom: 40px;
}
.section table {
  border-spacing: 30px 15px;
}
.section th {
  width: 100px;
  text-align: center;
  padding: 5px;
}
.section th input {
  width: 100%;
}
.section th button {
  width: 100%;
}
.packages td {
  padding: 5px;
}
.modules td {
  width: 500px;
  padding: 5px;
}
.jobs .grid-container {
  display: grid;
  grid-template-rows: repeat(5, auto);
  grid-auto-flow: column;
  grid-gap: 10px;
  width: 300px;
}
.jobs .grid-item {
  width: 300px;
  color: gray;
  font-weight: bold;
  font-family: monospace;
  padding: 5px;
}
.jobs .running {
  color: green;
  font-weight: bold;
}
.help {
  width: 650px;
}
.footer {
  color: blue;
}
a:link {
  text-decoration: none;
  color: inherit;
}
a:hover {
  text-decoration: underline;
  color: inherit;
}
a:active {
  color: inherit;
}
a:visited {
  color: inherit;
}
hr {
  border: 1px solid gray;
}
</style>

<script type="text/javascript">
function sync_inputs(textbox) {
  var x = document.getElementsByClassName(textbox.className);
  for (var i = 0; i < x.length; i++) {
    x[i].value = textbox.value;
  }
}

function reset_form() {
  // This should reset everything except the password field.
  var p = document.getElementById("passcode");
  var value = p.value;
  document.getElementById("form").reset();
  p.value = value;
}
</script>
</head>

<?php
  putenv('LANG=en_US.UTF-8');

  $packages = file("packages.txt", FILE_IGNORE_NEW_LINES);
  $rvgl_packages = array(
    "rvgl_assets",
    "rvgl_win32",
    "rvgl_win64",
    "rvgl_linux",
    "rvgl_macos"
  );

  function get_logs() {
    $ignored = array(".", "..");
    $files = array();

    foreach (scandir("logs") as $file) {
      if (in_array($file, $ignored)) {
        continue;
      }
      $info = pathinfo("$file");
      if ($info["extension"] != "txt") {
        continue;
      }
      $files[$file] = filemtime("logs/$file");
    }

    arsort($files);
    $files = array_keys($files);
    return ($files) ? $files : false;
  }

  function get_version_file($pack) {
    global $rvgl_packages;
    if (in_array($pack, $rvgl_packages)) {
      return "assets/rvgl_version";
    }
    return "assets/$pack";
  }

  function get_pack_version($pack) {
    $file = get_version_file($pack);
    return trim(file_get_contents("$file.txt"));
  }

  function get_pack_lasttag($pack) {
    $file = get_version_file($pack);
    return trim(file_get_contents("$file.tag"));
  }

  function create_lock($job) {
    if (!file_exists(".lock")) {
      return file_put_contents(".lock", "$job\n");
    }
    return false;
  }

  function delete_lock() {
    return unlink(".lock");
  }

  function process_post() {
    global $passcode;
    global $packages;

    if (!isset($_POST['passcode']) || $_POST['passcode'] != $passcode) {
      return;
    }

    if (isset($_POST["update"])) {
      if ($packages && create_lock("version_update")) {
        foreach ($packages as $pack) {
          $file = get_version_file($pack);
          $ver = trim($_POST["version_$pack"]);
          $tag = trim($_POST["lasttag_$pack"]);
          file_put_contents("$file.txt", "$ver\n", LOCK_EX);
          file_put_contents("$file.tag", "$tag\n", LOCK_EX);
        }
        delete_lock();
      }
    }

    if (isset($_POST["build"])) {
      $time = strftime("%Y-%m-%d_%H-%M-%S");
      $job = "build_$time";
      $args = "";

      if (isset($_POST["rebuild"]) && !empty($_POST["rebuild"])) {
        foreach ($_POST["rebuild"] as $pack) {
          $args .= " --rebuild '$pack'";
        }
      }
      if (isset($_POST["rebuild_aio"])) {
        $args .= " --rebuild-aio";
      }
      if (isset($_POST["rebuild_releases"])) {
        $args .= " --rebuild-releases";
      }

      if (create_lock($job)) {
        $output = "logs/$job.txt";
        file_put_contents($output, "", LOCK_EX);
        shell_exec("python3 build_packages.py $args 1>>$output 2>>$output &");
      }
    }

    if (isset($_POST["status"]) || isset($_POST["sync"])) {
      $time = strftime("%Y-%m-%d_%H-%M-%S");
      $job = "status_$time";
      $args = "";

      if (isset($_POST["sync"])) {
        $job = "sync_$time";
        $args = "--sync";
      }

      if (create_lock($job)) {
        $output = "logs/$job.txt";
        file_put_contents($output, "", LOCK_EX);
        shell_exec("python3 sync_packages.py $args 1>>$output 2>>$output &");
      }
    }
  }

  $is_locked = false;
  $running_job = "";
  $disabled = "";

  if (file_exists(".lock")) {
    $is_locked = true;
    $running_job = trim(file_get_contents(".lock"));
    $disabled = "disabled";
  }

  if (!$is_locked) {
    process_post();
  }

  if (count($_POST)) {
    header("Location: " . $_SERVER['REQUEST_URI']);
    exit();
  }
?>

<body onload="reset_form()">

  <h2>Re-Volt I/O Distribution Control Suite <code><small>v<?php echo "$version"; ?></small></code></h2>

  <form id="form" enctype="multipart/form-data" action="control.php" method="POST">

    <div class='section'>
    <p><i>Authorized Personnel Only</i></p>
    <input id='passcode' type='password' name='passcode' placeholder='Enter Password' <?php echo "$disabled"; ?> />
    </div>

    <div class='section jobs'>
    <h3>Recent Jobs</h3>

    <div class='grid-container'>
<?php
  $log_files = get_logs();
  if ($log_files) {
    $recent = array_slice($log_files, 0, 10);
    foreach($recent as $file) {
      $job = basename($file, ".txt");
      if ($job == $running_job) {
        echo "<div class='grid-item running'><a href='logs/$file'>$job [Running]</a></div>";
      } else {
        echo "<div class='grid-item'><a href='logs/$file'>$job [Finished]</a></div>";
      }
    }
  }
?>
    </div>
    </div>

    <div class='section packages'>
    <h3>Available Packages</h3>

    <table>
<?php
  if ($packages) {
    echo "<tr><th>Name</th><th>Version</th><th>Last Tag</th><th>Rebuild</th></tr>";
    foreach ($packages as $pack) {
      $ver = get_pack_version($pack);
      $tag = get_pack_lasttag($pack);
      $is_rvgl = in_array($pack, $rvgl_packages);
      echo "<tr><th><code>$pack</code></th>";
      $class1 = $is_rvgl ? "class='version_rvgl' oninput='sync_inputs(this)'" : "";
      $class2 = $is_rvgl ? "class='lasttag_rvgl' oninput='sync_inputs(this)'" : "";
      echo "<td><input $class1 type='text' value='$ver' name='version_$pack' placeholder='YY.MMDD' $disabled /></td>";
      echo "<td><input $class2 type='text' value='$tag' name='lasttag_$pack' placeholder='Tag or Commit ID' $disabled /></td>";
      echo "<th><input type='checkbox' value='$pack' name='rebuild[]' $disabled /></th></tr>";
    }
    echo "<tr><th></th><th colspan='2'><input type='submit' value='Update' name='update' $disabled /></th><th></th></tr>";
    echo "<tr><th><code>All-In-One</code></th><th></th><th></th><th><input type='checkbox' value='yes' name='rebuild_aio' $disabled /></th></tr>";
    echo "<tr><th><code>Releases</code></th><th></th><th></th><th><input type='checkbox' value='yes' name='rebuild_releases' $disabled /></th></tr>";
  }
?>
    </table>
    </div>

    <div class='section modules'>
    <h3>Available Modules</h3>

    <table>
    <tr>
      <th><input type='submit' value='Status' name='status' <?php echo "$disabled"; ?> /></th>
      <td>View state of packages. Local content is untouched.</td>
    </tr>
    <tr>
      <th><input type='submit' value='Sync' name='sync' <?php echo "$disabled"; ?> /></th>
      <td>Synchronize packages with upstream. Local content is replaced with the specified version.</td>
    </tr>
    <tr>
      <th><input type='submit' value='Build' name='build' <?php echo "$disabled"; ?> /></th>
      <td>Build packages and releases. Generate All-In-One package.</td>
    </tr>
    </table>
    </div>

  </form>

  <div class='section help'>
  <h3>Operating Instructions</h3>

  <strong>Building packages</strong>
  <p>Press the <code>Build</code> button. This will:</p>
  <ul>
    <li>Build packages that are missing or out of date.</li>
    <li>Build all-in-one update package.</li>
    <li>Build full game releases.</li>
  </ul>
  <p>To force a rebuild, use the checkboxes beside each package.</p>

  <strong>Updating packages</strong>
  <p>Package content is pulled directly from GitLab. Follow these steps:</p>
  <ul>
    <li>Ensure that the repo's master branch is up to date and version tagged.
    The tag must be in <code>YY.MMDD</code> format (eg, <code>20.0123</code>).</li>
    <li>Under Available Packages section:
      <ul>
        <li>Clear out the <code>Last Tag</code> entries from every package.</li>
        <li>For each package that must be updated, copy the <code>Version</code>
        number to <code>Last Tag</code> and then update the <code>Version</code>.</li>
        <li>Press the <code>Update</code> button.</li>
      </ul>
    </li>
    <li>Under Available Modules section:
      <ul>
        <li>Press <code>Sync</code> button to fetch latest assets. Wait for the
        process to complete (refresh page from time to time) and check the log under
        Recent Jobs for any errors.</li>
        <li>Press <code>Build</code> button to start building packages, releases
        and all-in-one package.</li>
      </ul>
    </li>
  </ul>

  <strong>Altering packages</strong>
  <ul>
    <li><strong>Adding a package:</strong> Ask a server administrator to add the
    new package definition to the source files. These blocks must be edited:
      <ul>
        <li>build_packages.py: <code>packages</code> and <code>releases</code>.</li>
        <li>sync_packages.py: <code>repositories</code>.</li>
      </ul>
    </li>
    <li>Press <code>Build</code> to regenerate the packages list.</li>
    <li>Fill in the package <code>Version</code>. For <code>Last Tag</code>,
    specify the keyword <code>INIT</code>. This will cause the entire package to
    be added to the all-in-one update.</li>
    <li>Press <code>Sync</code> to initiate the repository cloning procedure.</li>
    <li>Press <code>Build</code> again to start the actual building.</li>
  </ul>
  <ul>
    <li><strong>Removing a package:</strong> Ask a server administrator to remove
    the package entries from the source files and clear out the package files.</li>
    <li>Press <code>Build</code> to regenerate the packages list.</li>
  </ul>
  <ul>
    <li><strong>Renaming a package:</strong> The simplest way to do this is by
    removing the package and then adding it afresh. Alternatively, a server
    administrator could do the following:
      <ul>
        <li>Rename the package definition in source files.</li>
        <li>Rename the package folder and package.txt in assets.</li>
        <li>Rename the package file in packs and package.txt in releases.</li>
        <li>Press <code>Build</code> to regenerate the packages list.</li>
      </ul>
    </li>
  </ul>
  </div>

  <hr/>
  <div class='section footer'>
  <p><a href='https://distribute.re-volt.io'>https://distribute.re-volt.io</a></p>
  </div>

</body>
</html>
