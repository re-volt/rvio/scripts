#!/usr/bin/python3

import os, sys
import subprocess
import datetime
import argparse
import json
import hashlib
import re


packages = {
    "game_files": "Core assets needed to play the game.",
    "soundtrack": "Original soundtrack (based on Dreamcast version).",
    "io_cars": "Additional cars created by the community. Used in regular I/O races.",
    "io_cars_bonus": "Additional cars created by the community. Reserved for special and unofficial events.",
    "io_clockworks": "The three original Acclaim Clockworks mixed together with thirteen Clockworks by RV_Passion.",
    "io_clockworks_modern": "Twelve original Clockworks by RV_Pure.",
    "io_lms": "Content for the unofficial Last Man Standing game mode created by the community.",
    "io_loadlevel": "Loading screens for custom tracks created by Balesz.",
    "io_music": "Extra music for the I/O track pack.",
    "io_skins": "Additional skins for cars included in the io_cars package.",
    "io_skins_bonus": "Additional skins for cars included in the io_cars_bonus package.",
    "io_stunts": "Additional Stunt Arena content created by the community.",
    "io_tag": "Additional Battle Tag content created by the community.",
    "io_tracks": "Additional tracks created by the community. Used in regular I/O races.",
    "io_tracks_bonus": "Additional tracks created by the community. Reserved for special and unofficial events.",
    "io_tracks_circuit": "Additional custom tracks used in drifting or other special events.",
    "io_soundtrack": "Free soundtrack alternative made by the community.",
    "rvgl_assets": "Common data files for RVGL.",
    "rvgl_win32": "Windows binaries for RVGL (32-bit).",
    "rvgl_win64": "Windows binaries for RVGL (64-bit).",
    "rvgl_linux": "GNU/Linux binaries for RVGL (x86, x86_64, armhf, aarch64).",
    "rvgl_macos": "macOS binaries for RVGL (x86_64 and arm64).",
    "rvgl_dcpack": "Dreamcast content pack for RVGL (Rooftops and additional cars).",
}


io_packages = [
    "io_cars",
    "io_music",
    "io_skins",
    "io_tracks",
    "io_soundtrack"
]


rvgl_packages = [
    "rvgl_assets",
    "rvgl_win32",
    "rvgl_win64",
    "rvgl_linux",
    "rvgl_macos"
]


releases = [
  {
    "name": "win32_original",
    "packages": ["game_files", "soundtrack", "rvgl_assets", "rvgl_win32", "rvgl_dcpack"],
    "description": "RVGL for Windows (32-bit). Includes original soundtrack."
  },
  {
    "name": "win64_original",
    "packages": ["game_files", "soundtrack", "rvgl_assets", "rvgl_win64", "rvgl_dcpack"],
    "description": "RVGL for Windows (64-bit). Includes original soundtrack."
  },
  {
    "name": "win32_basic",
    "packages": ["game_files", "rvgl_assets", "rvgl_win32", "rvgl_dcpack"],
    "description": "RVGL for Windows (32-bit). Does not include the soundtrack."
  },
  {
    "name": "win64_basic",
    "packages": ["game_files", "rvgl_assets", "rvgl_win64", "rvgl_dcpack"],
    "description": "RVGL for Windows (64-bit). Does not include the soundtrack."
  },
  {
    "name": "win32_online",
    "packages": ["game_files", "rvgl_assets", "rvgl_win32", "rvgl_dcpack"] + io_packages,
    "description": "RVGL for Windows (32-bit). Includes community soundtrack and additional content for playing online."
  },
  {
    "name": "win64_online",
    "packages": ["game_files", "rvgl_assets", "rvgl_win64", "rvgl_dcpack"] + io_packages,
    "description": "RVGL for Windows (64-bit). Includes community soundtrack and additional content for playing online."
  },
  {
    "name": "linux_original",
    "packages": ["game_files", "soundtrack", "rvgl_assets", "rvgl_linux", "rvgl_dcpack"],
    "description": "RVGL for GNU/Linux (x86, x86_64, armhf, aarch64). Includes original soundtrack."
  }
]


special_releases = [
  {
    "name": "macos_original", "ext": "dmg",
    "packages": ["game_files", "soundtrack", "rvgl_assets", "rvgl_dcpack"],
    "description": "RVGL for macOS (10.9+, Intel and Apple Silicon). Includes original soundtrack."
  },
  {
    "name": "android_original", "ext": "apk",
    "packages": ["game_files", "soundtrack", "rvgl_assets", "rvgl_dcpack"],
    "description": "RVGL for Android (x86, x86_64, armhf, aarch64). Includes original soundtrack."
  }
]


options = None


""" Convert a version string into a tuple """
def parse_version(version):
    try:
        groups = re.match(r"(\d+)(?:\.(\d+))?([a-z]+)?(?:(?:-|.)?(\d+))?", version).groups("")
        major = parse_int(groups[0])
        minor = parse_int(groups[1])
        revision = parse_int(groups[3])
        return (major, minor, revision)
    except Exception:
        return (0, 0, 0)


""" Convert a string into integer """
def parse_int(value):
    try:
        return int(value)
    except Exception:
        return 0


def get_version_file(package):
    if package in rvgl_packages:
        return "rvgl_version"
    else:
        return package


def get_version(package):
    try:
        version = get_version_file(package)
        with open("assets/{}.txt".format(version), "r") as f:
            return f.readline().strip()
    except:
        return ""


def get_last_tag(package):
    try:
        version = get_version_file(package)
        with open("assets/{}.tag".format(version), "r") as f:
            return f.readline().strip()
    except:
        return ""


def get_dist_version(package):
    try:
        version = get_version_file(package)
        with open("releases/{}.txt".format(version), "r") as f:
            return f.readline().strip()
    except:
        return ""


def check_version(package):
    ver = parse_version(get_version(package))
    distver = parse_version(get_dist_version(package))
    return ver > distver


def write_version(package):
    try:
        version = get_version_file(package)
        with open("assets/{}.txt".format(version), "r") as f:
            ver = f.readline()
        with open("releases/{}.txt".format(version), "w") as f:
            f.write(ver)
    except:
        return


def get_checksum(package):
    try:
        sha = hashlib.sha256()
        filename = "packs/{}.zip".format(package)
        with open(filename, 'rb') as f:
            while True:
                chunk = f.read(256*1024)
                if not chunk:
                    break
                sha.update(chunk)
        return sha.hexdigest()
    except Exception as e:
        return ""


def is_locked():
    return os.path.isfile(".lock")

def create_lock():
    if is_locked():
        return False
    os.system("touch .lock")
    return is_locked()

def delete_lock():
    os.remove(".lock")
    return not is_locked()


def run_command(command):
    out, err = subprocess.Popen(command, stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT, shell=True).communicate()
    return out.decode()


def repo_exists(package):
    return os.path.isdir("assets/{}/.git".format(package))


def write_packages():
    with open("packages.txt", "w") as f:
        f.write("\n".join(packages))

    packinfo = {}
    for package in packages:
        packinfo[package] = {}
        packinfo[package]["description"] = packages[package]
        packinfo[package]["version"] = get_dist_version(package)
        packinfo[package]["checksum"] = get_checksum(package)

    with open('packages.json', 'w') as f:
        json.dump(packinfo, f)


def write_page():
    now = datetime.datetime.now()

    if os.path.isfile("index.html"):
        os.remove("index.html")

    f = open("index.html", "a")
    f.write("<!DOCTYPE html>\n<html>\n<body>\n")

    f.write("<h1>RVGL Releases</h1>\n")
    f.write("<p>On this page you can find full game releases of RVGL and content packages for the game.<br>")
    f.write("Find out more about the downloads on <a href=\"https://re-volt.io/downloads\">re-volt.io/downloads</a></p>\n")

    f.write("<p>Generated on {}</p>\n".format(now.strftime("%Y-%m-%d %H:%M")))

    f.write("<h2>Available Releases:</h2>\n")
    f.write("<ul>\n")
    for release in releases + special_releases:
        f.write("  <li><a href=\"#{}\">{}</a></li>\n".format(release["name"], release["name"]))
    f.write("</ul>\n")

    f.write("<h2>Available Packs:</h2>\n")
    f.write("<ul>\n")
    for pack in packages:
        f.write("  <li><a href=\"#{}\">{}</a></li>\n".format(pack, pack))
    f.write("</ul>\n<hr>\n")

    f.write("<h2 id=\"releases\">Releases</h2>\n")
    for release in releases + special_releases:
        f.write("<h3 id=\"{}\">{}</h3>\n".format(release["name"], release["name"]))
        f.write("<a href=\"https://distribute.re-volt.io/releases/rvgl_full_{}.{}\">Download</a>".format(release["name"], release.get("ext", "zip")))
        f.write("<p class=\"description\">Description: <em>{}</em></p>".format(release["description"]))
        f.write("<p>Packages:</p>")
        f.write("<ul>\n")
        for package in release["packages"]:
            f.write("  <li>{} <code>[{}]</code></li>\n".format(package, get_dist_version(package)))
        f.write("</ul>\n")

    f.write("<hr>\n<h2 id=\"packs\">Packs</h2>\n")

    for package in packages:
        f.write("<h3 id=\"{}\">{}</h3>\n".format(package, package))
        f.write("<a href=\"https://distribute.re-volt.io/packs/{}.zip\">Download</a>".format(package))
        f.write("<p class=\"description\">Description: <em>{}</em></p>".format(packages[package]))
        f.write("<p>Version: <code>[{}]</code>".format(get_dist_version(package)))


    f.write("<hr>\n")
    f.write("<p><a href=\"https://rvgl.re-volt.io\">rvgl.re-volt.io</a></p>\n")
    f.write("</body>\n</html>\n")
    f.close()


def build_package(package):
    output = "packs/{}.zip".format(package)
    output_new = "packs/{}_new.zip".format(package)

    if not get_version(package):
        print("Package {} is not being built.".format(package))
        return

    if package not in options.rebuild:
        if not check_version(package) and os.path.isfile(output):
            print("Package {} is up to date.".format(package))
            return

    if os.path.isfile(output_new):
        os.remove(output_new)

    print("Creating {}...".format(output))
    command = "cd assets/{} && zip -r -9 '{}' *".format(package, os.path.abspath(output_new))
    out = run_command(command)
    print(out)

    if os.path.isfile(output_new):
        if os.path.isfile(output):
            os.remove(output)
        print("Renaming output...")
        os.rename(output_new, output)


def build_release(release):
    output = "releases/rvgl_full_{}.zip".format(release["name"])
    output_new = "releases/rvgl_full_{}_new.zip".format(release["name"])

    rebuild = options.rebuild_releases
    for package in release["packages"]:
        if check_version(package):
            rebuild = True
            break

    if not rebuild and os.path.isfile(output):
        print("Release {} is up to date.".format(release["name"]))
        return

    if os.path.isfile(output_new):
        os.remove(output_new)

    print("Creating {}...".format(output))
    command = "zipmerge {}".format(output_new)
    for package in release["packages"]:
        if os.path.isfile("packs/{}.zip".format(package)):
            print("  Adding package \"{}\"".format(package))
            command += " packs/{}.zip".format(package)
        else:
            print("  Skipping package \"{}\"".format(package))

    out = run_command(command)
    print(out)

    if os.path.isfile(output_new):
        if os.path.isfile(output):
            os.remove(output)
        print("Renaming output...")
        os.rename(output_new, output)


def build_aio_package():
    aio_packages = io_packages # [p for p in packages if p.startswith("io")]
    versions = [""]

    for package in aio_packages:
        versions.append(get_version(package))

    version = max(versions, key=parse_version)
    output = "packs/updates/io_update_{}.zip".format(version)
    output_new = "packs/updates/io_update_{}_new.zip".format(version)

    if not version:
        print("All-In-One Package is not being built.")
        return

    if not options.rebuild_aio and os.path.isfile(output):
        print("All-In-One Package is up to date.")
        return

    if os.path.isfile(output_new):
        os.remove(output_new)

    print("Creating All-In-One Package...")
    for package in aio_packages:
        tag = get_last_tag(package)

        if tag == "INIT":
            print("  Adding package \"{}\"".format(package))
            command = "cd assets/{} && zip -D -r -9 '{}' *".format(package, os.path.abspath(output_new))
            out = run_command(command)
            print(out)

        elif tag and repo_exists(package):
            print("  Adding package \"{}\"".format(package))
            # NOTE: Set 'git config core.quotepath false' for unicode filenames to work.
            command = "cd assets/{} && git diff-tree -r --no-commit-id --name-only --diff-filter=ACMRT " \
                "{}.. | zip -9 '{}' -@".format(package, tag, os.path.abspath(output_new))
            out = run_command(command)
            print(out)

    if os.path.isfile(output_new):
        if os.path.isfile(output):
            os.remove(output)
        print("Renaming output...")
        os.rename(output_new, output)


def main():
    now = datetime.datetime.now()
    print("Starting at {}".format(now.strftime("%Y-%m-%d %H:%M")))
    print("Using encoding: {}".format(sys.stdout.encoding))
    print("")

    if not os.path.isdir("releases"):
        os.mkdir("releases")

    if not os.path.isdir("packs"):
        os.mkdir("packs")

    for package in packages:
        build_package(package)

    for release in releases:
        build_release(release)

    for package in packages:
        write_version(package)

    build_aio_package()

    write_packages()
    write_page()

    now = datetime.datetime.now()
    print("Finished at {}".format(now.strftime("%Y-%m-%d %H:%M")))


parser = argparse.ArgumentParser()
parser.add_argument("--rebuild", action="append", default=[])
parser.add_argument("--rebuild-aio", action="store_true")
parser.add_argument("--rebuild-releases", action="store_true")
options = parser.parse_args()

# NOTE: Lock is created by the parent script.

#if not create_lock():
#    print("Could not create lock. Exiting.")
#    exit()

if not is_locked():
    print("Process was not locked. Exiting.")
    exit()

main()

delete_lock()
