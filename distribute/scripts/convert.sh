#!/bin/bash

shopt -s globstar

for file in **; do
  base="${file%.*}"
  ext="${file##*.}"
  if [[ "${ext,,}" = bm* && "$(identify -format "%m" "$file")" = BMP* ]]; then
    echo "Converting: $(file "$file")"
    convert "$file" "$base.png"
    if [ -s "$base.png" ]; then
      mv -f "$base.png" "$file"
      echo "            $(file "$file")"
    else
      rm -f "$base.png"
      echo "            $file: Conversion failed!"
    fi
  fi
done

echo "Done."
read -r -p "Press any key to continue." -n 1
echo
