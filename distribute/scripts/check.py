#!/usr/bin/env python

import sys
import os
import glob

content_types = ["cars", "levels", "gfx", "cups", "models", "music", "wavs"]


def list_dir(path):
  try:
    entries = next(os.walk(path))
    entries[1].sort() # folders first in sorted order
    entries[2].sort() # then files in sorted order
    return entries[1] + entries[2]
  except Exception as e:
    return []

def glob_dir(path):
  try:
    files = []
    entries = glob.glob(os.path.join(path, "**"), recursive=True)
    entries.sort()
    for e in entries:
      if not os.path.isdir(e):
        files.append(os.path.relpath(e, path))
    return files
  except Exception as e:
    return []

def in_pack(path, packs_list, packs_dir):
  pack_list = []
  for pack in packs_list:
    if os.path.exists(os.path.join(packs_dir, pack, path)):
      pack_list.append(pack)

  return sorted(pack_list)


def check_all(packs_list, packs_dir):
  content_list = {}
  for pack in packs_list:
    for typ in content_types:
      prefix = os.path.join(packs_dir, pack, typ)
      for entry in list_dir(prefix):
        path = os.path.join(typ, entry)
        if path not in content_list:
          content_list[path] = in_pack(path, packs_list, packs_dir)

  for path, packs in sorted(content_list.items()):
    if len(packs) > 1:
      base_dir = os.path.join(packs_dir, packs[0], path)
      if not os.path.isdir(base_dir):
        continue
      files_list = {}
      for pack in packs:
        prefix = os.path.join(packs_dir, pack, path)
        for entry in glob_dir(prefix):
          file_path = os.path.join(path, entry)
          if file_path not in content_list:
            files_list[entry] = in_pack(file_path, packs_list, packs_dir)

      if any(len(p) > 1 for _, p in files_list.items()):
        print(f"[{path}]", "=>", packs)

      for file_path, file_packs in sorted(files_list.items()):
        if len(file_packs) > 1:
          print(f"==> [{file_path}]", "=>", file_packs)



check_all([p for p in next(os.walk("."))[1]], ".")
