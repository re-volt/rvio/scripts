#!/bin/bash

shopt -s globstar

re="^\./[^/]*\.dll$\|^\./lib/.*\.so.*$|^\./licenses/.*$|^\./scripts/.*$"

echo "Fixing filenames..."

for file in **; do
  dest="./${file,,}"
  file="${dest%/*}"/"${file##*/}"
  if [[ "$file" != "$dest" && ! "$file" =~ $re ]]; then
    [ ! -e "$dest" ] && mv -T "$file" "$dest" || echo "$file was not renamed!"
  fi
done

echo "Done."
read -r -p "Press any key to continue." -n 1
echo
