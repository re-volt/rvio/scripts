#!/usr/bin/python3

import os, sys
import subprocess
import datetime
import argparse
import json
import hashlib
import re


folder_list = [
    "battle-tag",
    "bonus_cars",
    "bonus_skins",
    "bonus_tracks",
    "cars",
    "circuit_tracks",
    "clockworks_classic",
    "clockworks_modern",
    "lms",
    "loadlevel",
    "music",
    "ost",
    "skins",
    "stunt-arenas",
    "tracks"
]

branches = {
    "lms": "main",
    "stunt-arenas": "main"
}


parser = argparse.ArgumentParser()
parser.add_argument("--status", action="store_true", help="Run 'git status' for each repository.")
parser.add_argument("--fetch", action="store_true", help="Run 'git fetch' for each repository (handles tags).")
parser.add_argument("--merge", action="store_true", help="Run 'git merge' for each repository (against origin).")
parser.add_argument("--push", action="store_true", help="Run 'git push' for each repository (handles tags).")
parser.add_argument("--stage", action="store_true", help="Stage all changes in each repository.")
parser.add_argument("--commit", help="Commit staged changes in each repository with COMMIT message.")
parser.add_argument("--list-tag", action="store_true", help="List the tag corresponding to tip of each repository.")
parser.add_argument("--delete-tag", metavar="TAG", help="Delete the tag matching TAG from each repository.")
parser.add_argument("--tag", help="Tag the tip of each untagged repository with TAG.")
parser.add_argument("--reset", action="store_true", help="Run 'git reset --hard' for each repository.")
options = parser.parse_args()


def run_command(command):
    out, err = subprocess.Popen(command, stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT, shell=True).communicate()
    return out.decode()


def repo_exists(folder):
    return os.path.isdir(f"{folder}/.git")


for folder in folder_list:
    print(f"==> {folder} <==")
    branch = branches.get(folder, "master")
    command = None

    if options.tag or options.list_tag:
        command = f"cd {folder} && git describe --tags --exact-match"
        out = run_command(command)
        if "fatal" not in out:
            print(out)
            continue
        if options.list_tag:
            continue

    if options.status:
        command = f"cd {folder} && git status"
    elif options.fetch:
        command = f"cd {folder} && git fetch --all --tags --prune --prune-tags --force"
    elif options.merge:
        command = f"cd {folder} && git merge origin {branch}"
    elif options.push:
        command = f"cd {folder} && git push -u origin {branch} && git push -f --tags"
    elif options.stage:
        command = f"cd {folder} && git add -A"
    elif options.commit:
        command = f"cd {folder} && git commit -m '{options.commit}'"
    elif options.tag:
        command = f"cd {folder} && git tag {options.tag}"
    elif options.delete_tag:
        command = f"cd {folder} && git tag -d {options.delete_tag}"
    elif options.reset:
        command = f"cd {folder} && git reset --hard origin/{branch}"

    if command:
        out = run_command(command)
        print(out)

