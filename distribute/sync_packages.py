#!/usr/bin/python3

import os, sys
import subprocess
import datetime
import argparse

from utils import check_packages


repositories = {
    "game_files": "https://gitlab.com/re-volt/game_files",
    "soundtrack": "https://gitlab.com/re-volt/ost",
    "io_cars": "https://gitlab.com/re-volt/rvio/cars",
    "io_cars_bonus": "https://gitlab.com/re-volt/rvio/bonus_cars",
    "io_clockworks": "https://gitlab.com/re-volt/rvio/clockworks_classic",
    "io_clockworks_modern": "https://gitlab.com/re-volt/rvio/clockworks_modern",
    "io_lms": "https://gitlab.com/re-volt/rvio/lms",
    "io_loadlevel": "https://gitlab.com/re-volt/rvio/loadlevel",
    "io_music": "https://gitlab.com/re-volt/rvio/music",
    "io_skins": "https://gitlab.com/re-volt/rvio/skins",
    "io_skins_bonus": "https://gitlab.com/re-volt/rvio/bonus_skins",
    "io_stunts": "https://gitlab.com/re-volt/rvio/stunt-arenas",
    "io_tag": "https://gitlab.com/re-volt/rvio/battle-tag",
    "io_tracks": "https://gitlab.com/re-volt/rvio/tracks",
    "io_tracks_bonus": "https://gitlab.com/re-volt/rvio/bonus_tracks",
    "io_tracks_circuit": "https://gitlab.com/re-volt/rvio/circuit_tracks",
    "io_soundtrack": "https://gitlab.com/re-volt/rvio/ost",
    "rvgl_base": "https://gitlab.com/re-volt/rvgl-base"
}

branches = {
    "io_lms": "main",
    "io_stunts": "main"
}

rvgl_packages = {
    "rvgl_assets": "rvgl_base/distrib/assets",
    "rvgl_dcpack": "rvgl_base/distrib/dcpack",
    "rvgl_win32": "rvgl_base/distrib/platform/win32",
    "rvgl_win64": "rvgl_base/distrib/platform/win64",
    "rvgl_linux": "rvgl_base/distrib/platform/linux",
    "rvgl_macos": "rvgl_base/distrib/platform/macos"
}

rvgl_modules = (
    "distrib/assets",
    "distrib/dcpack",
    "distrib/platform"
)

status_clean = "working tree clean"


options = None


def is_locked():
    return os.path.isfile(".lock")

def create_lock():
    if is_locked():
        return False
    os.system("touch .lock")
    return is_locked()

def delete_lock():
    os.remove(".lock")
    return not is_locked()


def run_command(command):
    out, err = subprocess.Popen(command, stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT, shell=True).communicate()
    return out.decode()


def repo_exists(package):
    return os.path.isdir("assets/{}/.git".format(package))


def get_package_tag(package):
    try:
        with open("assets/{}.txt".format(package), "r") as f:
            version = f.readline().strip()
        if run_command("cd assets/{} && git tag -l {}".format(package, version)):
            return version
    except:
        pass

    branch = branches.get(package, "master")
    return "origin/{}".format(branch)


def get_rvgl_tag(package):
    try:
        with open("assets/rvgl_version.txt", "r") as f:
            version = f.readline().strip()
        if run_command("cd assets/{} && git tag -l {}".format(package, version)):
            return version
    except:
        pass

    return "origin/master"


def package_init(package):
    print("Initializing repository...")
    try:
        os.mkdir("assets/{}".format(package))
    except:
        pass

    if not os.path.isdir("assets/{}".format(package)):
        print("Package folder could not be created!")
        return False

    if len(os.listdir("assets/{}".format(package))) != 0:
        print("Package folder is not empty!")
        return False

    out = run_command("cd assets/{} && git clone {}.git . && " \
        "git status".format(package, repositories[package]))
    print(out)

    return True


def rvgl_init(package):
    if not package_init(package):
        return False

    modules = " ".join(rvgl_modules)
    out = run_command("cd assets/{} && git submodule init -- {} && "
        "git status".format(package, modules))
    print(out)

    out = run_command("cd assets/{} && bash bootstrap.sh".format(package))
    print(out)

    for pack in rvgl_packages:
        out = run_command("cd assets && ln -s {} {}".format(rvgl_packages[pack], pack))
        print(out)

    return True


def package_status(package):
    print("Package {}...".format(package))
    # NOTE: Set 'git config fetch.pruneTags true' to properly sync deleted tags.
    out = run_command("cd assets/{} && git fetch -P -f && git status".format(package))
    print(out)

    if not options.sync:
        return

    if not repo_exists(package) and not package_init(package):
        return

    if options.reset or status_clean not in out:
        out = run_command("cd assets/{} && git reset --hard".format(package))
        print(out)

    tag = get_package_tag(package)
    out = run_command("cd assets/{} && git checkout {} && git status".format(package, tag))
    print(out)


def rvgl_status(package):
    print("Package RVGL...")
    out = run_command("cd assets/{} && git fetch -P -f && " \
        "git submodule foreach git fetch -P -f && git status".format(package))
    print(out)

    if not options.sync:
        return

    if not repo_exists(package) and not rvgl_init(package):
        return

    if options.reset or status_clean not in out:
        out = run_command("cd assets/{} && git reset --hard && " \
            "git submodule foreach git reset --hard".format(package))
        print(out)

    tag = get_rvgl_tag(package)
    out = run_command("cd assets/{} && git checkout {} && " \
        "git submodule update && git status".format(package, tag))
    print(out)


def main():
    now = datetime.datetime.now()
    print("Starting at {}".format(now.strftime("%Y-%m-%d %H:%M")))
    print("Using encoding: {}".format(sys.stdout.encoding))
    print("")

    for package in repositories:
        if package.startswith("rvgl"):
            rvgl_status(package)
        else:
            package_status(package)

    io_packages = [p for p in repositories if p.startswith("io")]
    check_packages(io_packages)

    now = datetime.datetime.now()
    print("Finished at {}".format(now.strftime("%Y-%m-%d %H:%M")))


parser = argparse.ArgumentParser()
parser.add_argument("--sync", action="store_true")
parser.add_argument("--reset", action="store_true")
options = parser.parse_args()

# NOTE: Lock is created by the parent script.

#if not create_lock():
#    print("Could not create lock. Exiting.")
#    exit()

if not is_locked():
    print("Process was not locked. Exiting.")
    exit()

main()

delete_lock()
