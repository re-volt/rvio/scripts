#!/usr/bin/env python

import os, sys
import glob
import imghdr


def list_dir(path):
    try:
        entries = next(os.walk(path))
        entries[1].sort() # folders first in sorted order
        entries[2].sort() # then files in sorted order
        return entries[1] + entries[2]
    except Exception as e:
        return []


def glob_dir(path):
    try:
        files = []
        entries = glob.glob(os.path.join(path, "**"), recursive=True)
        entries.sort()
        for e in entries:
            if not os.path.isdir(e):
                files.append(os.path.relpath(e, path))
        return files
    except Exception as e:
        return []


def in_pack(path, packs_list):
    packages = []
    for pack in packs_list:
        if os.path.exists(os.path.join("assets", pack, path)):
            packages.append(pack)

    return sorted(packages)


def check_conflicts(packs_list):
    content_types = ["cars", "levels", "gfx", "cups", "models", "music", "wavs"]
    content_list = {}

    for pack in packs_list:
        for typ in content_types:
            prefix = os.path.join("assets", pack, typ)
            for entry in list_dir(prefix):
                path = os.path.join(typ, entry)
                if path not in content_list:
                    content_list[path] = in_pack(path, packs_list)

    for path, packs in sorted(content_list.items()):
        if len(packs) > 1:
            base_dir = os.path.join("assets", packs[0], path)
            if not os.path.isdir(base_dir):
                continue

            files_list = {}
            for pack in packs:
                prefix = os.path.join("assets", pack, path)
                for entry in glob_dir(prefix):
                    file_path = os.path.join(path, entry)
                    if file_path not in content_list:
                        files_list[entry] = in_pack(file_path, packs_list)

            if any(len(p) > 1 for _, p in files_list.items()):
                print(f"[{path}]", "=>", packs)
                for fpath, fpacks in sorted(files_list.items()):
                    if len(fpacks) > 1:
                        print(f"==> [{fpath}]", "=>", fpacks)


def check_cases(pack):
    prefix = os.path.join("assets", pack)
    for entry in glob_dir(prefix):
        if entry != entry.lower():
            print(pack, "=>", entry)


def check_textures(pack):
    prefix = os.path.join("assets", pack)
    for entry in glob_dir(prefix):
        _, ext = os.path.splitext(entry)
        if ext.startswith(".bm"):
            path = os.path.join(prefix, entry)
            if imghdr.what(path) == "bmp":
                print(pack, "=>", entry)


def check_packages(packs_list):
    print("Checking for conflicts...")
    check_conflicts(packs_list)
    print("")

    print("Checking for uppercase files...")
    for pack in packs_list:
        check_cases(pack)
    print("")

    print("Checking for bitmap textures...")
    for pack in packs_list:
        check_textures(pack)
    print("")
