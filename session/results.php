<?php
  //ini_set('display_errors', 'On');
  //error_reporting(E_ALL);
  $start = microtime(true);

  include('keys.php');

  // time converters
  function rvtimetoms($str) {
    $exp = explode(":", $str);
    return $exp[2] + $exp[1]*1000 + $exp[0]*60000;
  }
  function rvmstotime($ms) {
    $min = floor($ms / 60000);
    $ms = $ms % 60000;
    $sec = floor($ms / 1000);
    $ms = $ms % 1000;
    return sprintf("%02d", $min).":".sprintf("%02d", $sec).":".sprintf("%03d", $ms);
  }
  function rvmstotimeshort($ms) {
    $min = floor($ms / 60000);
    $ms = $ms % 60000;
    $sec = floor($ms / 1000);
    $ms = $ms % 1000;
    $string = "+";
    if($min > 0) $string .= $min.":".sprintf("%02d", $sec);
    else $string .= $sec;
    return $string.":".sprintf("%03d", $ms);
  }

  // sort functions
  function sortbyscore($array) {
    // sort drivers by score
    for($i = (count($array)-1); $i >= 0; $i--) {
      for($j = $i; $j >= 0; $j--) {
        if(($array[$i]['score'] > $array[$j]['score'])||(($array[$i]['score'] == $array[$j]['score'])&&($array[$i]['wins'] > $array[$j]['wins']))||(($array[$i]['score'] == $array[$j]['score'])&&($array[$i]['wins'] == $array[$j]['wins'])&&($array[$i]['name'] > $array[$j]['name']))) {
          $tmp = $array[$j];
          $array[$j] = $array[$i];
          $array[$i] = $tmp;
          $j = $i;
        }
      }
    }
    return $array;
  }

  function sortbywins($array) {
    // sort drivers by wins
    for($i = (count($array)-1); $i >= 0; $i--) {
      for($j = $i; $j >= 0; $j--) {
        if(($array[$i]['wins'] > $array[$j]['wins'])||(($array[$i]['wins'] == $array[$j]['wins'])&&($array[$i]['score'] > $array[$j]['score']))||(($array[$i]['wins'] == $array[$j]['wins'])&&($array[$i]['score'] == $array[$j]['score'])&&($array[$i]['name'] > $array[$j]['name']))) {
          $tmp = $array[$j];
          $array[$j] = $array[$i];
          $array[$i] = $tmp;
          $j = $i;
        }
      }
    }
    return $array;
  }

  function sortbytime($array) {
    // sort drivers by time
    for($i = 0; $i < count($array); $i++) {
      for($j = 0; $j < $i; $j++) {
        if($array[$i]['time'] < $array[$j]['time']) {
          $tmp = $array[$j];
          $array[$j] = $array[$i];
          $array[$i] = $tmp;
          $j = 0;
        }
      }
    }
    return $array;
  }

  // title dictionary
  function readnames() {
    $dict = [];
    if(($handle = fopen("names.csv", "r")) !== false) {
      while(($data = fgetcsv($handle, 1000, ",")) !== false) {
        if(count($data) > 1 && $data[0] !== "") {
          $dict[$data[0]] = $data[1];
        }
      }
      fclose($handle);
    }
    return $dict;
  }

  function updatenames($file, $name) {
    $dict = readnames();
    if($name === false) unset($dict[$file]);
    else $dict[$file] = $name;

    if(($handle = fopen("tmp.csv", "w")) !== false) {
      foreach ($dict as $key => $value) {
        if (fputcsv($handle, array($key, $value)) === false) {
          fclose($handle);
          unlink("tmp.csv");
          return $dict;
        }
      }
      fclose($handle);
      unlink("names.csv");
      rename("tmp.csv", "names.csv");
    }

    return $dict;
  }

  // version checker
  function checkversion($ver, $maj, $min) {
    if(is_array($ver) && count($ver) >= 3) {
      if(($ver[1] > $maj) || ($ver[1] == $maj && $ver[2] >= $min)) {
        return true;
      }
    }
    return false;
  }

  // vars
  $rvgl = 0; // version (YY.MMDD)
  $rvgl_text = 0; // complete version text

  $session = [];
  $races = [];
  $drivers = [];
  $cars = [];
  $podium = [];

  $r = 0;

  $session['rand'] = true;
  $session['onecar'] = false;
  $session['single'] = false;
  $session['duel'] = false;
  $randomcar = "";

  $empty = "-";
  $maxtime = 0;

  $mlang = array(
    array("Simulation", "Simulatie", "Szimuláció", "Simulazione", "Symulacja", "Simulação", "Simulación", "Simulator", "Simuliacija"),
    array("Arcade", "Arcadie", "Árkád", "Zrecznosciowy", "Arkad", "Arkada"),
    array("Console", "Konsole", "Konzol", "Konsolowy", "Consola", "Konsoll", "Konsolė"),
    array("Junior RC", "Junior-RC")
  );

  // populate title dictionary
  $names = readnames();

  // upload files
  if(isset($_POST['upload'])) {
    if($_FILES['file']['name'][0] === "") {
      echo "Please select a file.<br><a href='?'>Go back</a>";
      return;
    }
    if((!isset($_POST['pass']))||(!((password_verify($_POST['pass'], $sects[$_POST['sects']]['psw']))||(password_verify($_POST['pass'], $master_psw))))) {
      echo "Wrong password.<br><a href='?'>Go back</a>";
      return;
    }
    // join files if needed
    if(count($_FILES['file']['name']) > 1) {
      //for($i = 0; $i < count($_FILES['file']['name']); $i++) echo $_FILES['file']['name'][$i]."<br>";
      for($i = 0; $i < count($_FILES['file']['name']); $i++) {
        // sort
        for($j = 0; $j < $i; $j++) {
          if($_FILES['file']['name'][$i] < $_FILES['file']['name'][$j]) {
            $tmp = $_FILES['file']['name'][$j];
            $_FILES['file']['name'][$j] = $_FILES['file']['name'][$i];
            $_FILES['file']['name'][$i] = $tmp;
            $tmp = $_FILES['file']['tmp_name'][$j];
            $_FILES['file']['tmp_name'][$j] = $_FILES['file']['tmp_name'][$i];
            $_FILES['file']['tmp_name'][$i] = $tmp;
            $j = 0;
          }
        }
      }
      /*
      echo "<br>";
      for($i = 0; $i < count($_FILES['file']['name']); $i++) echo $_FILES['file']['name'][$i]."<br>";
      echo "<br>";
      */
      if(($handle2 = fopen("tmp.csv", "w")) !== false) {
        for($i = 0; $i < count($_FILES['file']['name']); $i++) {
          if(($handle = fopen($_FILES['file']['tmp_name'][$i], "r")) !== false) {
            $data = fgetcsv($handle, 1000, ",");
            if(($data === false) || ((strcmp($data[0], "Session") !== 0)&&(strcmp($data[0], "Version") !== 0))) {
              echo $_FILES['file']['name'][$i]." is not RVGL related.<br><a href='?'>Go back</a>";
              fclose($handle);
              fclose($handle2);
              unlink("tmp.csv");
              return;
            }
            fputcsv($handle2, $data);
            while(($data = fgetcsv($handle, 1000, ",")) !== false) {
              fputcsv($handle2, $data);
            }
            fclose($handle);
          }
        }
        fclose($handle2);
        unlink($_FILES['file']['tmp_name'][0]);
        rename("tmp.csv", $_FILES['file']['tmp_name'][0]);
      }
    }
    $file = "./results/".$sects[$_POST['sects']]['dir'].basename($_FILES['file']['name'][0]);
    if((strpos($_FILES['file']['name'][0], $sects[$_POST['sects']]['accept']) !== 0)&&(strcmp($sects[$_POST['sects']]['accept'], "") !== 0)) {
      echo "This file doesn't respect the requirements (start with \"".$sects[$_POST['sects']]['accept']."\").<br><a href='?'>Go back</a>";
      return;
    }
    if(file_exists($file)) {
      echo "This file already exists.<br><a href='?'>Go back</a>";
      return;
    }
    if(strcmp(strtolower(pathinfo($file, PATHINFO_EXTENSION)), "csv") !== 0) {
      echo "Uploaded file is not a CSV.<br><a href='?'>Go back</a>";
      return;
    }
    if(($handle = fopen($_FILES['file']['tmp_name'][0], "r")) !== false) {
      $data = fgetcsv($handle, 1000, ",");
      fclose($handle);
      if(($data === false) || ((strcmp($data[0], "Session") !== 0)&&(strcmp($data[0], "Version") !== 0))) {
        echo "Your CSV file is not RVGL related.<br><a href='?'>Go back</a>";
        return;
      }
    } else {
      echo "Can't check file.<br><a href='?'>Go back</a>";
      return;
    }
    if(move_uploaded_file($_FILES['file']['tmp_name'][0], $file)) {
      if(strcmp($_POST['name'], "") !== 0) $names = updatenames($sects[$_POST['sects']]['dir'].basename($_FILES['file']['name'][0]), $_POST['name']);
      echo "The file ".basename($_FILES['file']['name'][0])." has been uploaded.<br><a href='?file=".$sects[$_POST['sects']]['dir'].basename($_FILES['file']['name'][0])."'>View results</a> or <a href='?'>Go back</a><br><br>";
      $tmp = glob("./results/".$sects[$_POST['sects']]['dir']."*.csv");
      if(!$sects[$_POST['sects']]['asc']) $tmp = array_reverse($tmp);
      if((count($tmp) > $sects[$_POST['sects']]['limit'])&&($sects[$_POST['sects']]['limit'] != 0)) {
        for($i = $sects[$_POST['sects']]['limit']; $i < count($tmp); $i++) {
          if(unlink($tmp[$i])) {
            echo "File ".$tmp[$i]." has been deleted (limit reached).<br>";
            $tmparr = explode("/", $tmp[$i]);
            $tmp[$i] = end($tmparr);
            $names = updatenames($sects[$_POST['sects']]['dir'].$tmp[$i], false);
          } else echo "Something went wrong deleting the file ".$tmp[$i].".<br>";
        }
      }
    } else {
      echo "Sorry, there was an error uploading your file.<br><a href='?'>Go back</a>";
    }
    return;
  }

  // edit files or delete
  if(isset($_POST['edit'])) {
    if((!isset($_POST['pass']))||(!password_verify($_POST['pass'], $master_psw))) {
      echo "Wrong edit password.<br><a href='?file=".$_POST['file']."'>Go back</a>";
      return;
    }
    if(strcmp($_POST['submit'], "Update") === 0) {
      if(strcmp($_POST['name'], "") !== 0) $names = updatenames($_POST['file'], $_POST['name']);
      else $names = updatenames($_POST['file'], false);
      echo "File ".$_POST['file']." has been edited.<br><a href='?file=".$_POST['file']."'>Go back</a>";
    } else if(strcmp($_POST['submit'], "Delete") === 0) {
      if(unlink("./results/".$_POST['file'])) {
        $names = updatenames($_POST['file'], false);
        echo "File ".$_POST['file']." has been deleted.<br><a href='?'>Go back to home</a>";
      } else echo "Something went wrong deleting the file.<br><a href='?file=".$_POST['file']."'>Go back</a>";
    }
    return;
  }

  // read CSV and store
  if(isset($_GET['file'])) {
    if(($handle = fopen("./results/".$_GET['file'], "r")) !== false) {
      $plsstop = false;
      while(($data = fgetcsv($handle, 1000, ",")) !== false) {
        if(strcmp($data[0], "Session") === 0) {
          if(!$plsstop) {
            $plsstop = true;
            $session['date'] = $data[1];
            $session['host'] = $data[2];
            if($data[5] == "true") $session['pick'] = "Enabled";
            else $session['pick'] = "Disabled";
            for($i = 0; $i < 4; $i++) {
              for($j = 0; $j < count($mlang[$i]); $j++) {
                if(strcmp($data[3], $mlang[$i][$j]) === 0) $session['mode'] = $mlang[$i][0];
              }
            }
          }
          $session['laps'] = $data[4];
        } else if(strcmp($data[0], "Version") === 0) {
          preg_match('/(\d+)(?:\.(\d+))?([a-z]+)?(?:(?:-|.)?(\d+))?/', $data[1], $rvgl);
          $rvgl_text = $data[1];
          $session['conn-type'] = $data[2];
          $session['conn-ag'] = $data[3];
        } else if(strcmp($data[0], "Results") === 0) {
          $r++;
          $races[$r][0]['name'] = $data[1];
          $races[$r][0]['laps'] = $session['laps'];
          $nr = $data[2];
          $pos = 0;
        } else if(strcmp($data[0], "#") !== 0) {
          $pos++;
          if($pos == 1) {
            //$races[$r][17] = rvtimetoms($data[3]);
            $randomcar = $data[2];
          }
          if($data[2] !== $randomcar) $session['rand'] = false;
          if(checkversion($rvgl, 19, 320)) $finished = $data[5]; // best time fix
          else $finished = $data[4];
          if($finished === "true") {
            $races[$r][$pos]['name'] = $data[1];
            $races[$r][$pos]['car'] = $data[2];
            $races[$r][$pos]['time'] = rvtimetoms($data[3]);
            $races[$r][$pos]['score'] = $nr;
            if(checkversion($rvgl, 19, 320)) $races[$r][$pos]['besttime'] = rvtimetoms($data[4]); // best time fix
            $nr--;
          } else {
            $pos--;
          }
        }
      }
      if(count($races) === 1) $session['rand'] = false;
      fclose($handle);
    } else die("The file you were looking for is not there. <a href='?'>Go back to home</a>");

    if(count($races) == 1) $session['single'] = true;

    // dump races results into stats
    for($r = 1; $r <= count($races); $r++) {
      for($i = 1; $i < count($races[$r]); $i++) {
        // drivers stats
        $done = false;
        for($j = 0; $j < count($drivers); $j++) {
          if(strcmp($drivers[$j]['name'], $races[$r][$i]['name']) === 0) {
            $done = true;
            $drivers[$j]['time'] += $races[$r][$i]['time'];
            $drivers[$j]['score'] += $races[$r][$i]['score'];
            if($i == 1) $drivers[$j]['wins']++;
            $drivers[$j]['races']++;
            //if((strpos($drivers[$j]['car'], $races[$r][$i]['car']) === false)&&(!$session['rand'])) $drivers[$j]['car'] .= "<br>".$races[$r][$i]['car'];
            if (!$session['rand']){
              if(!in_array($races[$r][$i]['car'], $drivers[$j]['car'])) {
                $drivers[$j]['car'][count($drivers[$j]['car'])] = $races[$r][$i]['car'];
              }
            }
          }
        }
        if(!$done) {
          $drivers[$j]['name'] = $races[$r][$i]['name'];
          if($session['rand']) $drivers[$j]['car'][0] = "Random";
          else $drivers[$j]['car'][0] = $races[$r][$i]['car'];
          $drivers[$j]['score'] = $races[$r][$i]['score'];
          if($i == 1) $drivers[$j]['wins'] = 1;
          else $drivers[$j]['wins'] = 0;
          $drivers[$j]['time'] = $races[$r][$i]['time'];
          $drivers[$j]['races'] = 1;
        }
        // cars stats
        $done = false;
        for($j = 0; $j < count($cars); $j++) {
          if(strcmp($cars[$j]['name'], $races[$r][$i]['car']) === 0) {
            $done = true;
            $cars[$j]['score'] += $races[$r][$i]['score'];
            if($i == 1) $cars[$j]['wins']++;
            $cars[$j]['races']++;
            if(strpos($cars[$j]['driver'],$races[$r][$i]['name']) === false) $cars[$j]['driver'] .= "<br>".$races[$r][$i]['name'];
          }
        }
        if(!$done) {
          $cars[$j]['name'] = $races[$r][$i]['car'];
          $cars[$j]['driver'] = $races[$r][$i]['name'];
          $cars[$j]['score'] = $races[$r][$i]['score'];
          if($i == 1) $cars[$j]['wins'] = 1;
          else $cars[$j]['wins'] = 0;
          $cars[$j]['races'] = 1;
        }
      }
    }

    // onecar fix
    if(count($cars) == 1) {
      $session['onecar'] = true;
      $session['rand'] = false;
      for($i = 0; $i < count($drivers); $i++) {
        $drivers[$i]['car'][0] = $cars[0]['name'];
      }
    }

    if(count($drivers) == 2) $session['duel'] = true;

    // increase time
    for($i = 0; $i < count($drivers); $i++) {
      for($r = 1; $r <= count($races); $r++) {
        $maxtime = $races[$r][0]['laps']*30000;
        $done = false;
        for($j = 1; $j < count($races[$r]); $j++) {
          if(strcmp($drivers[$i]['name'], $races[$r][$j]['name']) === 0) {
            $done = true;
          }
        }
        if(!$done) {
          $drivers[$i]['time'] += $races[$r][$j-1]['time']+$maxtime;
        }
      }
    }
    $drivers = sortbyscore($drivers);
  }

  if(isset($_GET['generate'])) {
    $tmparr = explode("/", $_GET['file']);
    header("Content-type: text/csv");
    header("Content-Disposition: attachment; filename=parsed-".end($tmparr));
    echo "Parsed,\"".$session['date']."\",\"".$session['host']."\",\"".$session['mode']."\",".$session['laps'].",\"".$session['pick']."\"\n";
    echo "Score\n";
    echo "#,Player,Cars";
    for($r = 1; $r <= count($races); $r++) echo ",\"".$races[$r][0]['name']."\"";
    echo ",Wins\n";
    for($i = 0; $i < count($drivers); $i++) {
      echo ($i+1).",\"".$drivers[$i]['name']."\",\"".implode(",", $drivers[$i]['car'])."\"";
      for($r = 1; $r <= count($races); $r++) {
        $done = false;
        for($j = 1; $j < count($races[$r]); $j++) {
          if(strcmp($drivers[$i]['name'], $races[$r][$j]['name']) === 0) {
            $done = true;
            echo ",".$races[$r][$j]['score'];
          }
        }
        if(!$done) echo ",0";
      }
      echo ",".$drivers[$i]['wins']."\n";
    }
    echo "Time\n";
    echo "#,Player,Cars";
    for($r = 1; $r <= count($races); $r++) echo ",\"".$races[$r][0]['name']."\"";
    echo "\n";
    $drivers = sortbytime($drivers);
    for($i = 0; $i < count($drivers); $i++) {
      echo ($i+1).",\"".$drivers[$i]['name']."\",\"".implode(",", $drivers[$i]['car'])."\"";
      for($r = 1; $r <= count($races); $r++) {
        $done = false;
        for($j = 1; $j < count($races[$r]); $j++) {
          if(strcmp($drivers[$i]['name'], $races[$r][$j]['name']) === 0) {
            $done = true;
            echo ",".$races[$r][$j]['time'];
          }
        }
        if(!$done) echo ",".($races[$r][$j-1]['time']+$maxtime);
      }
      echo "\n";
    }
    return;
  }

  $title = "RVGL Session Parser";
  $description = "Makes your RVGL session.csv a lot cooler!";

  if(isset($_GET['file'])) {
    $ftitle = $names[$_GET['file']] ?? false;
    if($ftitle === false) $title .= " - ".$_GET['file'];
    else $title .= " - ".$ftitle;
    $description = date("F j, Y", strtotime(explode("_", $_GET['file'])[1]))."\n\n";
    $description .= "Top Score:\n\t1. ".$drivers[0]['name']." (".$drivers[0]['score']." pts)\n\t2. ".$drivers[1]['name']." (".$drivers[1]['score']." pts)\n\t3. ".$drivers[2]['name']." (".$drivers[2]['score']." pts)";
    /*
    $drivers = sortbytime($drivers);
    $description .= "\n\nTop Time:\n\t1. ".$drivers[0]['name']." (".rvmstotime($drivers[0]['time']).")\n\t2. ".$drivers[1]['name']." (".rvmstotime($drivers[1]['time']).")\n\t3. ".$drivers[2]['name']." (".rvmstotime($drivers[2]['time']).")";
    $drivers = sortbyscore($drivers);
    $drivers = sortbyscore($drivers);
    */
    if($session['duel']) {
      $drivers = sortbywins($drivers);
      $description = $drivers[0]['name']." ".$drivers[0]['wins']." - ".$drivers[1]['wins']." ".$drivers[1]['name'];
    }
    if($session['single']) {
      $drivers = sortbytime($drivers);
      $description = $drivers[0]['name']." (".rvmstotime($drivers[0]['time']).")\n2. ".$drivers[1]['name']." (".rvmstotime($drivers[1]['time']).")\n3. ".$drivers[2]['name']." (".rvmstotime($drivers[2]['time']).")";
    }
  }
?>

<html>
  <head>
    <title><?=$title?></title>
    <meta name='description' content='<?=$description?>'>
    <meta name='theme-color' content='#50b050'>
    <meta property='og:title' content='<?php
      if(isset($_GET['file'])) {
        $ftitle = $names[$_GET['file']] ?? false;
        if($ftitle === false) echo $_GET['file']." by ".$session['host'];
        else echo $ftitle;
      } else echo "RVGL Session Parser";
    ?>'>
    <meta property='og:site_name' content='<?php if(isset($_GET['file'])) echo "RVGL Session Parser";?>'>
    <meta property='og:image' content='http://jc27.altervista.org/re-volt/icons/rv-csv-icon.png'>
    <meta property='og:description' content='<?=$description?>'>
    <link rel="stylesheet" type="text/css" href="./style/volty.css">
    <!-- link rel="stylesheet" type="text/css" href="./style/scroll.css" -->
    <link rel="icon" type="image/png" href="./icons/rv-csv-icon.png">
    <style>
      html {
        height: 100%;
      }
      body {
        font-size: 16px;
        min-height: 100%;
        position: relative;
        margin: 0px;
      }
      table {
        background: initial;
        transition: all 0.15s linear;
        border-collapse: collapse;
      }
      th {
        text-align: center;
      }
      table, th, td {
        padding: 5px;
      }
      .racer {
        background: initial;
        transition: all 0.15s linear;
      }
      .notrans {
        transition: none !important;
      }
      .racer:hover, .select2 {
        cursor: pointer;
      }
      .vertical {
        height: 175px;
        width: 30px;
        text-align: left;
      }
      .vertical span {
        writing-mode: vertical-rl;
        text-align: right;
        line-height: 95%;
        display: inline;
        vertical-align: bottom;
      }
      td.time, td.car {
        font-size: 12px;
      }
      td.car > span {
        display: inline-block;
        max-height: 30px;
        overflow: hidden;
        position: relative;
      }
      td.car > span::before {
        content: "...";
        position: absolute;
        top: 19px;
        left: 5px;
      }
      td.car > span:hover {
        max-height: 1000px;
      }
      td.car > span:hover::before {
        display: none;
      }
      table.podium, table.podium tr, table.podium td {
        margin: 0px;
        padding: 0px;
        border: 0px;
        text-align: center;
        height: 20px;
        white-space: nowrap;
      }
      table.podium {
        width: 300px;
      }
      table.podium td {
        width: 100px;
      }
      td.decor {
        vertical-align: top;
        height: 20px;
        width: 100px;
      }
      td a { display: block; }
      th.paginator > span {
        display: inline-block;
        width: 15px;
      }
      th.paginator > button {
        width: 35px;
      }
      input[name=name], input[name=file] { width: 200px; }
      .container {
        display: block;
        padding: 5px;
      }
      .footer {
        position: absolute;
        bottom: 0px;
        font-size: 80%;
      }
      .select-, .select-blue, .select-red, .select-green, .select-yellow, .select-cyan, .select-purple {
        transition: all 0.15s linear;
      }
      .clearer: { clear: none; }
      .clearer:nth-of-type(2n) { clear: none; }
      .clearer:nth-of-type(3n) { clear: none; }
      .clearer:nth-of-type(4n-2) { clear: both; }
      @media only screen and (max-width: 1800px) {
        .clearer:nth-of-type(2n) { clear: none; }
        .clearer:nth-of-type(4n-2) { clear: none; }
        .clearer:nth-of-type(3n) { clear: both; }
      }
      @media only screen and (max-width: 1200px) {
        .clearer:nth-of-type(3n) { clear: none; }
        .clearer:nth-of-type(4n-2) { clear: none; }
        .clearer:nth-of-type(2n) { clear: both; }
      }
      label ~ .fst, label ~ .snd, label ~ .trd, label ~ .lst {
        display: none;
        font-size: 70%;
      }
    </style>
    <script>
      var colors = [
        ["", "blue"],
        ["", "red"],
        ["", "green"],
        ["", "yellow"],
        ["", "purple"],
        ["", "cyan"]
      ];

      var hilink = document.createElement("link");
      hilink.setAttribute("rel", "stylesheet");
      hilink.setAttribute("type", "text/css");
      hilink.setAttribute("href", "./style/hi.css?ver=1.14");

      function SelectAll(str) {
        var x = document.getElementsByClassName("racer " + str);
        var c = "";
        var i;
        var j = -1;
        for(i = 0; i < colors.length; i++) {
          if(colors[i][0] == str) {
            c = colors[i][1];
            j = i;
            break;
          }
        }
        if((c == "")&&(x[0].classList.contains('select-') == false)) {
          for(i = 0; i < colors.length; i++) {
            if(colors[i][0] == "") {
              c = colors[i][1];
              colors[i][0] = str;
              j = i;
              break;
            }
          }
        }
        for(i = 0; i < x.length; i++) {
          if(x[i].classList.contains('select-'+c)) {
            x[i].classList.remove('select-'+c);
            if(x[i].classList.contains('hide1')) x[i].style.display = 'table-row';
            if(x[i].classList.contains('hide2')) x[i].style.display = 'none';
            if(j > -1) colors[j][0] = "";
          } else {
            x[i].classList.add('select-'+c);
            if(x[i].classList.contains('hide1')) x[i].style.display = 'none';
            if(x[i].classList.contains('hide2')) x[i].style.display = 'table-row';
          }
        }
      }

      function GoTo(str) {
        location.href = "#" + str;
        var x = document.getElementById(str);
        x.classList.add('select2');
        setTimeout(function(){ x.classList.remove('select2'); }, 300);
      }

      function changeCSS(cssFile) {
        var oldlink = document.getElementsByTagName("link").item(0);
        var newlink = document.createElement("link");
        newlink.setAttribute("rel", "stylesheet");
        newlink.setAttribute("type", "text/css");
        newlink.setAttribute("href", cssFile);
        document.getElementsByTagName("head").item(0).replaceChild(newlink, oldlink);
        sessionStorage.setItem("css", cssFile);
      }

      function checkHi() {
        var head = document.getElementsByTagName("head").item(0);
        var check = document.getElementById("hi");
        if(check.checked) head.appendChild(hilink);
        else head.removeChild(head.lastChild);
      }

      function toggleEdit() {
        var editor = document.getElementById("editor");
        if(editor.style.display == "none") editor.style.display = "inline-block";
        else editor.style.display = "none";
      }

      function prefixDate() {
        var file = document.getElementById("file").value.split("\\").pop();
        var parts = file.split("_");
        var d = new Date(parts[1]);
        if(!isNaN(d.getTime())) {
          var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
          document.getElementById("name").value = /* months[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear() + " - " + */ parts[0].charAt(0).toUpperCase()+ parts[0].slice(1);
        } else {
          document.getElementById("name").value = "";
        }
      }


      if(sessionStorage.getItem("css") !== null) changeCSS(sessionStorage.getItem("css"));
    </script>
  </head>
  <body>
    <span class='container'>
      <div style='float: right'>
        Stylesheet:
        <a href='#' onclick='changeCSS("./style/light.css");'>Light</a> -
        <a href='#' onclick='changeCSS("./style/dark.css");'>Dark</a> -
        <a href='#' onclick='changeCSS("./style/volty.css");'>Volty</a>
      </div>
<?php
  // file list
  if(!isset($_GET['file'])) {
    $pagelimit = 15;
    echo "<div style='clear: both;'></div>";
    for($i = 0; $i < count($sects); $i++) {
      echo "<span class='volty' style='float: left; margin: 0px 20px 50px 0px'><table class='filelist' id='t".$i."'><tr><th colspan=2>".$sects[$i]['name']."</th></tr>";
      $tmp = glob("./results/".$sects[$i]['dir']."*.csv");
      if(!$sects[$i]['asc']) $tmp = array_reverse($tmp);
      $longername = " ";
      foreach($tmp as $file) {
        $file = explode("/", $file);
        $file = end($file);
        $ftitle = $names[$sects[$i]['dir'].$file] ?? false;
        if($ftitle === false) $ftitle = $file;
        if(strlen($ftitle) > strlen($longername)) $longername = $ftitle;
        $fdate = date("F j, Y", (isset(explode("_", $file)[1]) ? strtotime(explode("_", $file)[1]) : strtotime("1999-03-09")));
        echo "<tr class='racer'><td><a href='?file=".$sects[$i]['dir'].$file."'><img src='./icons/rv-csv-icon.png' style='width: 16px; vertical-align: sub'> ".$ftitle."</a></td><td class='car'>".$fdate."</td></tr>";
      }
      if(count($tmp) > $pagelimit) {

        for($j = count($tmp); $j % $pagelimit; $j++) echo "<tr class='racer'><td>-</td><td></td></tr>";
        echo "<tr style='visibility: collapse; font-weight: bolder'><th><img src='./icons/rv-csv-icon.png' style='width: 16px; vertical-align: sub'> ".$longername."</th><th style='width: 120px'></td></tr>";
        $total = ceil(count($tmp)/$pagelimit);
        echo "<tr><th colspan=2 class='paginator'><button onclick=\"CP('t".$i."', -".$total.");\">|&lt;</button><button onclick=\"CP('t".$i."', -1)\">&lt;</button><span id='t".$i."p1'>1</span>/<span id='t".$i."p2'>".$total."</span><button onclick=\"CP('t".$i."', 1)\">&gt;</button><button onclick=\"CP('t".$i."', ".$total.")\">&gt;|</button></th></tr>";
      }
      echo "</table></span>";
    }
    ?>
      <span class='volty'>
        <form action='' method='post' enctype='multipart/form-data'>
          <table>
            <tr>
              <th colspan=2>Upload</th>
            </tr>
            <tr>
              <td>File</td>
              <td><input type="file" id="file" name="file[]" accept=".csv, text/csv" onchange="prefixDate();" multiple></td>
            </tr>
            <tr>
              <td>Title</td>
              <td><input type="text" id="name" name="name"></td>
            </tr>
            <tr>
              <td>Section</td>
              <td>
                <select name='sects'>
                  <?php
                    for($i = 0; $i < count($sects); $i++) {
                      echo "<option value=".$i.">".$sects[$i]['dir']."</option>";
                    }
                  ?>
                </select>
              </td>
            </tr>
            <tr>
              <td>Password</td>
              <td><input type="password" name="pass"></td>
            </tr>
            <tr>
              <td><input type="hidden" name="upload" value=1></td>
              <td><input type="submit" value="Upload" name="submit"></td>
            </tr>
          </table>
        </form>
      </span>
      <script>
        var page = 0;
        var x = document.getElementsByClassName('filelist');
        var i;
        for(i = 0; i < x.length; i++) {
          var y = x[i].getElementsByTagName('td');
          var j;
          for(j = 0; j < y.length; j++) {
            if(Math.floor(j/<?=($pagelimit*2)?>) == page) {
              y[j].style.display = 'table-cell';
            } else {
              y[j].style.display = 'none';
            }
          }
        }
        function CP(table, dir) {
          var t = document.getElementById(table);
          var curp = Number(document.getElementById(table+'p1').innerHTML);
          var totp = Number(document.getElementById(table+'p2').innerHTML);
          curp = curp + dir;
          if(curp < 1) curp = 1;
          if(curp > totp) curp = totp;
          var c = t.getElementsByTagName('td');
          var i;
          for(i = 0; i < c.length; i++) {
            if(Math.floor(i/<?=($pagelimit*2)?>) == (curp-1)) {
              c[i].style.display = 'table-cell';
            } else {
              c[i].style.display = 'none';
            }
          }
          document.getElementById(table+'p1').innerHTML = curp;
        }
      </script>
      <br>
      <!--<div class='footer'>Session parser by JohnCorl.</div>-->
    <?php
    return;
  }

  // print sessions info
  ?>
    <h1>
      <img src='./icons/rv-csv-icon.png' style='width: 32px; vertical-align: sub'>
      <?php
        if($ftitle !== false) echo $ftitle."<br><span style='font-size: 50%; font-weight: normal'>".$_GET['file']."</span>";
        else echo $_GET['file'];
      ?>
    </h1>
    <div class='volty' style='float: left; margin-right: 20px'>
      <h3>
        <?php
          if($rvgl) echo "<b>Version:</b> ".$rvgl_text."<br>";
          if(isset($session['conn-type'])) echo "<b>Connection:</b> ".$session['conn-type']. " (".$session['conn-ag'].")<br>";
          if($rvgl) echo "<br>";
        ?>
        <b>Date:</b> <?=date("F j, Y", strtotime(explode("_", $_GET['file'])[1]))?><br>
        <b>Host:</b> <?=$session['host']?><br>
        <b>Mode:</b> <?=$session['mode']?><br>
        <b>Pickups:</b> <?=$session['pick']?>
      </h3>
    </div>
    <?php
      if($session['single']) {
        $drivers = sortbytime($drivers);
    ?>
      <div style='float: left; padding: 0px 20px 50px 0px;'>
        <h2>Podium</h2>
        <table class='podium'>
          <tr>
            <td rowspan=2></td>
            <td <?php if(strlen($drivers[0]['name']) > 10) echo "class='time'";?>><?=$drivers[0]['name']?></td>
            <td rowspan=4></td>
          </tr>
          <tr>
            <td class='decor racer <?=$drivers[0]['name']?>' onclick='SelectAll("<?=$drivers[0]['name']?>")' rowspan=6><?=rvmstotime($drivers[0]['time'])?></td>
          </tr>
          <tr>
            <td <?php if(strlen($drivers[1]['name']) > 10) echo "class='time'";?>><?=$drivers[1]['name']?></td>

          </tr>
          <tr>
            <td class='decor racer <?=$drivers[1]['name']?>' onclick='SelectAll("<?=$drivers[1]['name']?>")' rowspan=4><?=rvmstotime($drivers[1]['time'])?></td>
          </tr>
          <tr>
            <td <?php if(strlen($drivers[2]['name']) > 10) echo "class='time'";?>><?=$drivers[2]['name']?></td>
          </tr>
          <tr>
            <td class='decor racer <?=$drivers[2]['name']?>' onclick='SelectAll("<?=$drivers[2]['name']?>")' rowspan=2><?=rvmstotime($drivers[2]['time'])?></td>
          </tr>
          <tr>
            <td style='width:0px'></td>
          </tr>
        </table>
      </div>
    <?php
        echo "<div style='clear:both'><h2>Race results</h2></div><span class='volty' style='float: left; margin: 0px 20px 50px 0px;'><table id='r".(1)."'><tr><th colspan='".(checkversion($rvgl, 19, 320) ? "6" : "5")."'>".$races[1][0]['name']." - ".$races[1][0]['laps']." laps"."</th></tr><tr><th>#</th><th>Name</th><th>Car</th><th>Time</th><th>Split</th>".(checkversion($rvgl, 19, 320) ? "<th>Best Lap</th>" : "")."</tr>";
        $first = 0;
        if(isset($races[1][1]['besttime'])) {
          $best = $races[1][1]['besttime'];
          for($j = 1; $j < count($races[1]); $j++) {
            if($races[1][$j]['besttime'] < $best) $best = $races[1][$j]['besttime'];
          }
        }
        for($j = 1; $j < count($races[1]); $j++) {
          if($j == 1) {
            $first = $races[1][$j]['time'];
            $split = $empty;
          } else {
            $split = rvmstotimeshort($races[1][$j]['time']-$first);
          }
          $mini = "";
          if(strlen($races[1][$j]['name']) > 10) $mini = " class='time'";
          echo "<tr class='racer ".$races[1][$j]['name']." ".$races[1][$j]['car']."' onclick=\"SelectAll('".$races[1][$j]['name']."')\"><td>".$j."</td><td".$mini.">".$races[1][$j]['name']."</td><td class='car'>".$races[1][$j]['car']."</td><td>".($j == 1 ? "<span class='fst'>" : "").rvmstotime($races[1][$j]['time'])."</td><td>".$split."</td>".(checkversion($rvgl, 19, 320) ? "<td>".($best == $races[1][$j]['besttime'] ? "<span class='fst'>" : "").rvmstotime($races[1][$j]['besttime'])."</td>" : "")."</tr>";
        }
        echo "</table></span><div class='clearer'></div>";
      } else if(!$session['duel']) {
    ?>
    <div style='float: left; padding-left: 20px;'>
      <h2>Score podium</h2>
      <table class='podium'>
        <tr>
          <td rowspan=2></td>
          <td <?php if(strlen($drivers[0]['name']) > 10) echo "class='time'";?>><?=$drivers[0]['name']?></td>
          <td rowspan=4></td>
        </tr>
        <tr>
          <td class='decor racer <?=$drivers[0]['name']?>' onclick='SelectAll("<?=$drivers[0]['name']?>")' rowspan=6><?=$drivers[0]['score']?> points</td>
        </tr>
        <tr>
          <td <?php if(strlen($drivers[1]['name']) > 10) echo "class='time'";?>><?=$drivers[1]['name']?></td>
        </tr>
        <tr>
          <td class='decor racer <?=$drivers[1]['name']?>' onclick='SelectAll("<?=$drivers[1]['name']?>")' rowspan=4><?=$drivers[1]['score']?> points</td>
        </tr>
        <tr>
          <td <?php if(strlen($drivers[2]['name']) > 10) echo "class='time'";?>><?=$drivers[2]['name']?></td>
        </tr>
        <tr>
          <td class='decor racer <?=$drivers[2]['name']?>'  onclick='SelectAll("<?=$drivers[2]['name']?>")' rowspan=2><?=$drivers[2]['score']?> points</td>
        </tr>
        <tr>
          <td style='width:0px'></td>
        </tr>
      </table>
    </div>
  <?php
    // sort by time
    $drivers = sortbytime($drivers);
  ?>
    <div style='float: left; padding-left: 20px;'>
      <h2>Time podium</h2>
      <table class='podium'>
        <tr>
          <td rowspan=2></td>
          <td <?php if(strlen($drivers[0]['name']) > 10) echo "class='time'";?>><?=$drivers[0]['name']?></td>
          <td rowspan=4></td>
        </tr>
        <tr>
          <td class='decor racer <?=$drivers[0]['name']?>' onclick='SelectAll("<?=$drivers[0]['name']?>")' rowspan=6><?=rvmstotime($drivers[0]['time'])?></td>
        </tr>
        <tr>
          <td <?php if(strlen($drivers[1]['name']) > 10) echo "class='time'";?>><?=$drivers[1]['name']?></td>

        </tr>
        <tr>
          <td class='decor racer <?=$drivers[1]['name']?>' onclick='SelectAll("<?=$drivers[1]['name']?>")' rowspan=4><?=rvmstotime($drivers[1]['time'])?></td>
        </tr>
        <tr>
          <td <?php if(strlen($drivers[2]['name']) > 10) echo "class='time'";?>><?=$drivers[2]['name']?></td>
        </tr>
        <tr>
          <td class='decor racer <?=$drivers[2]['name']?>' onclick='SelectAll("<?=$drivers[2]['name']?>")' rowspan=2><?=rvmstotime($drivers[2]['time'])?></td>
        </tr>
        <tr>
          <td style='width:0px'></td>
        </tr>
      </table>
    </div>

  <?php
    // sort by wins
    $drivers = sortbywins($drivers);
  ?>
    <div style='float: left; padding-left: 20px;'>
      <h2>Wins podium</h2>
      <table class='podium'>
        <tr>
          <td rowspan=2></td>
          <td <?php if(strlen($drivers[0]['name']) > 10) echo "class='time'";?>><?=$drivers[0]['name']?></td>
          <td rowspan=4></td>
        </tr>
        <tr>
          <td class='decor racer <?=$drivers[0]['name']?>' onclick='SelectAll("<?=$drivers[0]['name']?>")' rowspan=6><?=$drivers[0]['wins']?> wins</td>
        </tr>
        <tr>
          <td <?php if(strlen($drivers[1]['name']) > 10) echo "class='time'";?>><?=$drivers[1]['name']?></td>
        </tr>
        <tr>
          <td class='decor racer <?=$drivers[1]['name']?>' onclick='SelectAll("<?=$drivers[1]['name']?>")' rowspan=4><?=$drivers[1]['wins']?> wins</td>
        </tr>
        <tr>
          <td <?php if(strlen($drivers[2]['name']) > 10) echo "class='time'";?>><?=$drivers[2]['name']?></td>
        </tr>
        <tr>
          <td class='decor racer <?=$drivers[2]['name']?>' onclick='SelectAll("<?=$drivers[2]['name']?>")' rowspan=2><?=$drivers[2]['wins']?> wins</td>
        </tr>
        <tr>
          <td style='width:0px'></td>
        </tr>
      </table>
    </div>
    <div style='clear: both; height: 50px;'></div>
  <?php
    // score ladder
    echo "<span class='volty scroll' style='margin: 0px 20px 50px 0px;'><h2>Score ladder</h2><span class='scroll-inner'><table><tr style='vertical-align: bottom'><th>#</th><th>Name</th><th>Car</th>";
    for($r = 1; $r <= count($races); $r++) echo "<th class='racer vertical' onclick=\"GoTo('r".$r."')\"><span>".$races[$r][0]['name']."</span></th>";
    echo "<th>Total</th><th>Wins</th><th>Avg</th></tr>";

    $drivers = sortbyscore($drivers);
    //$drivers = sortbyscore($drivers);

    for($i = 0; $i < count($drivers); $i++) {
      $mini = "";
      $more = "";
      if(count($drivers[$i]['car']) > 1) $more = "hide1";
      if(strlen($drivers[$i]['name']) > 10) $mini = " class='time'";
      echo "<tr class='racer ".$drivers[$i]['name']." ".$more."' onclick=\"SelectAll('".$drivers[$i]['name']."')\"><td>".($i+1)."<td".$mini.">".$drivers[$i]['name']."</td><td class='car'><span>";
      echo $drivers[$i]['car'][0];
      for($c = 1; $c < count($drivers[$i]['car']); $c++) echo "<br>".$drivers[$i]['car'][$c];
      echo "</span></td>";
      for($r = 1; $r <= count($races); $r++) {
        $done = false;
        echo "<td>";
        for($j = 1; $j < count($races[$r]); $j++) {
          if(strcmp($drivers[$i]['name'], $races[$r][$j]['name']) === 0) {
            $done = true;
            if($j == 1) echo "<b><span class='fst'>".$races[$r][$j]['score']."</span></b>";
            else if($j == 2) echo "<span class='snd'>".$races[$r][$j]['score']."</span>";
            else if($j == 3) echo "<span class='trd'>".$races[$r][$j]['score']."</span>";
            else if($j == count($races[$r])-1) echo "<span class='lst'>".$races[$r][$j]['score']."</span>";
            else echo $races[$r][$j]['score'];
          }
        }
        if(!$done) echo $empty;
        echo "</td>";
      }
      if($i == 0) $mini1 = "<span class='fst'>";
      else if($i == 1) $mini1 = "<span class='snd'>";
      else if($i == 2) $mini1 = "<span class='trd'>";
      else $mini1 = "<span>";
      echo "<td>".$mini1.$drivers[$i]['score']."</span></td>";
      $tmpn = $drivers[$i]['name'];

      $drivers = sortbywins($drivers);
      if(strcmp($drivers[0]['name'], $tmpn) === 0) $mini2 = "<span class='fst'>";
      else if(strcmp($drivers[1]['name'], $tmpn) === 0) $mini2 = "<span class='snd'>";
      else if(strcmp($drivers[2]['name'], $tmpn) === 0) $mini2 = "<span class='trd'>";
      else $mini2 = "<span>";
      $drivers = sortbyscore($drivers);
      //$drivers = sortbyscore($drivers);

      //$mini2 = "<span>";
      $wins = $drivers[$i]['wins'] > 0 ? $drivers[$i]['wins'] : $empty;
      echo "<td>".$mini2.$wins."</span></td><td>".round($drivers[$i]['score']/$drivers[$i]['races'], 2)."</td></tr>";
      // more car
      if(count($drivers[$i]['car']) > 1) {
        echo "<tr style='display: none' class='racer ".$drivers[$i]['name']." hide2' onclick=\"SelectAll('".$drivers[$i]['name']."')\"><td rowspan=".(count($drivers[$i]['car'])+1).">".($i+1)."</td><td ".$mini." rowspan=".(count($drivers[$i]['car'])+1).">".$drivers[$i]['name']."</td>";
        for($c = 0; $c < count($drivers[$i]['car']); $c++) {
          $total = 0;
          $wins = 0;
          $count = 0;
          echo "<tr style='display: none' class='racer ".$drivers[$i]['name']." hide2' onclick=\"SelectAll('".$drivers[$i]['name']."')\"><td class='car'>".$drivers[$i]['car'][$c]."</td>";
          for($r = 1; $r <= count($races); $r++) {
            $done = false;
            echo "<td class='car'>";
            for($j = 1; $j < count($races[$r]); $j++) {
              if((strcmp($drivers[$i]['name'], $races[$r][$j]['name']) === 0)&&(strcmp($drivers[$i]['car'][$c], $races[$r][$j]['car']) === 0)) {
                $done = true;
                $total += $races[$r][$j]['score'];
                if($j == 1) $wins++;
                $count++;
                if($j == 1) echo "<b><span class='fst'>".$races[$r][$j]['score']."</span></b>";
                else if($j == 2) echo "<span class='snd'>".$races[$r][$j]['score']."</span>";
                else if($j == 3) echo "<span class='trd'>".$races[$r][$j]['score']."</span>";
                else if($j == count($races[$r])-1) echo "<span class='lst'>".$races[$r][$j]['score']."</span>";
                else echo $races[$r][$j]['score'];
              }
            }
            if(!$done) echo $empty;
            echo "</td>";
          }
          if($wins == 0) $wins = $empty;
          echo "<td class='car'>".$total."</td><td class='car'>".$wins."</td><td class='car'>".round($total/$count, 2)."</td></tr>";
        }
      }
    }
    echo "<tr><td colspan=3><input type='checkbox' id='hi' onclick='checkHi();'><label for='hi' style='cursor: pointer; font-size: 80%'> highlights</label><br><span class='fst'>First</span> <span class='snd'>Second</span> <span class='trd'>Third</span> <span class='lst'>Last</span></td><td colspan=".(count($races)+3)." class='car'>First place gains 1 point for every ";
    if(count($races) < 4) echo "<br>";
    echo "racer connected at race start.</td></tr></table></span></span>";

    // time ladder
    echo "<span class='volty scroll' style='margin: 0px 20px 50px 0px;'><h2>Time ladder</h2><span class='scroll-inner'><table><tr style='vertical-align: bottom'><th>#</th><th>Name</th>";//<th>Car</th>";
    for($r = 1; $r <= count($races); $r++) echo "<th class='racer vertical' onclick=\"GoTo('r".$r."')\"><span>".$races[$r][0]['name']."</span></th>";
    echo "<th>Total time</th><th>Split</th></tr>";

    $drivers = sortbytime($drivers);

    for($i = 0; $i < count($drivers); $i++) {
      $mini = "";
      if(strlen($drivers[$i]['name']) > 10) $mini = " class='time'";
      echo "<tr class='racer ".$drivers[$i]['name']."' onclick=\"SelectAll('".$drivers[$i]['name']."')\"><td>".($i+1)."<td".$mini.">".$drivers[$i]['name']."</td>";//<td>".$drivers[$i]['car']."</td>";
      for($r = 1; $r <= count($races); $r++) {
        $done = false;
        echo "<td class='time'>";
        for($j = 1; $j < count($races[$r]); $j++) {
          if(strcmp($drivers[$i]['name'], $races[$r][$j]['name']) === 0) {
            $done = true;
            $showtime = $races[$r][$j]['time']-$races[$r][1]['time'];
            if($showtime > 0) echo rvmstotimeshort($showtime);
            else echo "<b><span class='fst'>".rvmstotime($races[$r][$j]['time'])."</span></b>";
          }
        }
        if(!$done) echo "<i>(".rvmstotimeshort($races[$r][$j-1]['time']-$races[$r][1]['time']+$maxtime).")</i>";
        echo "</td>";
      }
      echo "<td>".rvmstotime($drivers[$i]['time'])."</td>";
      if($i === 0) echo "<td>".$empty."</td>";
      else echo "<td>".rvmstotimeshort($drivers[$i]['time']-$drivers[0]['time'])."</td>";
      echo "</tr>";
    }
    echo "<tr><td></td><td></td><td colspan=".(count($races)+2)." class='car'>Penality time added for disconnected racers: ";
    if(count($races) < 6) echo "<br>";
    echo "last place time +30:000 for every lap.</td></tr>";
    echo "</table></span></span><br>";

  } else {
    // duel
    echo "<span class='volty' style='margin: 0px 20px 50px 0px;'><table><tr style='vertical-align: bottom'><th>#</th><th>Name</th><th>Car</th>";
    for($r = 1; $r <= count($races); $r++) echo "<th class='racer vertical' style='height: 100px' onclick=\"GoTo('r".$r."')\"><span>".$races[$r][0]['name']."</span></th>";
    echo "<th>Wins</th></tr>";

    $drivers = sortbywins($drivers);

    for($i = 0; $i < 2; $i++) {
      $mini = "";
      if(strlen($drivers[$i]['name']) > 10) $mini = " class='time'";
      echo "<tr class='racer ".$drivers[$i]['name']."' onclick=\"SelectAll('".$drivers[$i]['name']."')\"><td>".($i+1)."<td".$mini.">".$drivers[$i]['name']."</td><td class='car'><span>";
      echo $drivers[$i]['car'][0];
      for($c = 1; $c < count($drivers[$i]['car']); $c++) echo "<br>".$drivers[$i]['car'][$c];
      echo "</span></td>";
      for($r = 1; $r <= count($races); $r++) {
        $done = false;
        echo "<td>";
        for($j = 1; $j < count($races[$r]); $j++) {
          if(strcmp($drivers[$i]['name'], $races[$r][$j]['name']) === 0) {
            $done = true;
            if($j == 1) echo "<b><span class='fst'>W</span></b>";
            else if($j == 2) echo "<span class='snd'>L</span>";
          }
        }
        if(!$done) echo "<span class='snd'>L</span>";
        echo "</td>";
      }
      echo "<td>".$drivers[$i]['wins']."</td></tr>";
    }
    echo "</table></span><div style='clear: both; height: 50px;'></div>";
  }

  // car ladder
  if((!$session['rand'])&&(!$session['onecar'])) {
    echo" <span class='volty'><h2>Cars stats</h2><table><tr style='vertical-align: bottom'><th>#</th><th>Name</th><th>Driver</th><th>Score</th><th>Races</th><th>Wins</th><th>Avg</th>";

    // sort by avg
    for($i = count($cars)-1; $i >= 0; $i--) {
      for($j = $i; $j >= 0; $j--) {
        if(($cars[$i]['score']/$cars[$i]['races']) > ($cars[$j]['score']/$cars[$j]['races'])) {
          $tmp = $cars[$j];
          $cars[$j] = $cars[$i];
          $cars[$i] = $tmp;
          $j = $i;
        }
      }
    }

    for($i = 0; $i < count($cars); $i++) {
      echo "<tr class='racer ".$cars[$i]['name']."' onclick=\"SelectAll('".$cars[$i]['name']."')\"><td>".($i+1)."<td>".$cars[$i]['name']."</td><td class='car'><span>".$cars[$i]['driver']."</td><td>".$cars[$i]['score']."</span></td><td>".$cars[$i]['races']."</td>";
      if($cars[$i]['wins'] > 0) echo "<td>".$cars[$i]['wins']."</td>";
      else echo "<td>".$empty."</td>";
      echo "<td>".round($cars[$i]['score']/$cars[$i]['races'], 2)."</td></tr>";
    }
    echo "</table></span>";
  }

  // single races
  if(!$session['single']) {
    echo "<h2>Single races</h2>";
    for($r = 1; $r <= count($races); $r++) {
      echo "<span class='volty' style='float: left; margin: 0px 20px 50px 0px;'><table style='max-width: 380px' id='r".$r."'><tr><th colspan='".(checkversion($rvgl, 19, 320) ? "6" : "5")."'>".$races[$r][0]['name']." - ".$races[$r][0]['laps']." laps"."</th></tr><tr><th>#</th><th>Name</th><th>Car</th><th>Time</th><th>Split</th>".(checkversion($rvgl, 19, 320) ? "<th>Best Lap</th>" : "")."</tr>";
      $first = 0;
      if(isset($races[$r][1]['besttime'])) {
        $best = $races[$r][1]['besttime'];
        for($j = 1; $j < count($races[$r]); $j++) {
          if($races[$r][$j]['besttime'] < $best) $best = $races[$r][$j]['besttime'];
        }
      }
      for($j = 1; $j < count($races[$r]); $j++) {
        if($j == 1) {
          $first = $races[$r][$j]['time'];
          $split = $empty;
        } else {
          $split = rvmstotimeshort($races[$r][$j]['time']-$first);
        }
        $mini = "";
        if(strlen($races[$r][$j]['name']) > 10) $mini = " class='time'";
        echo "<tr class='racer ".$races[$r][$j]['name']." ".$races[$r][$j]['car']."' onclick=\"SelectAll('".$races[$r][$j]['name']."')\"><td>".$j."</td><td".$mini.">".$races[$r][$j]['name']."</td><td class='car'>".$races[$r][$j]['car']."</td><td>".($j == 1 ? "<span class='fst'>" : "").rvmstotime($races[$r][$j]['time'])."</td><td>".$split."</td>".(checkversion($rvgl, 19, 320) ? "<td>".($best == $races[$r][$j]['besttime'] ? "<span class='fst'>" : "").rvmstotime($races[$r][$j]['besttime'])."</td>" : "")."</tr>";
      }
      echo "</table></span><div class='clearer'></div>";
    }
  }

  echo "<div style='clear: both'></div><div class='footer'>Session parser by JohnCorl, ".round((microtime(true)-$start),15)."s. <a href='#editor' onclick='toggleEdit();'>Edit</a> - <a href='?'>Go back</a> - <a href='./results/".$_GET['file']."'>View raw data</a> - <a href='?file=".$_GET['file']."&generate'>Download parsed data</a></div>";
?>
      <span class='volty' id='editor' style='display: none'>
        <form action='' method='post' enctype='multipart/form-data'>
          <table>
            <tr>
              <th colspan=2>Edit</th>
            </tr>
            <tr>
              <td>File</td>
              <td><?=$_GET['file']?></td>
            </tr>
            <tr>
              <td>Title</td>
              <td><input type="text" name="name" value="<?=$ftitle?>"></td>
            </tr>
            <tr>
              <!--
              <td>Section</td>
              <td>
                <select name='sects'>
                  <?php
                    for($i = 0; $i < count($sects); $i++) {
                      echo "<option value=".$i.">".$sects[$i]['dir']."</option>";
                    }
                  ?>
                </select>
              </td>
              -->
            </tr>
            <tr>
              <td>Password</td>
              <td><input type="password" name="pass"></td>
            </tr>
            <tr>
              <td>
                <input type="hidden" name="edit" value=1>
                <input type="hidden" name="file" value="<?=$_GET['file']?>">
              </td>
              <td><input type="submit" value="Update" name="submit"> or <input type="submit" value="Delete" name="submit"></td>
            </tr>
          </table>
        </form>
      </span>
    </span>
  </body>
  <script>document.getElementById('hi').checked = false;</script>
</html>
