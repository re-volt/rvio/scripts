<?php
  /*
    Here you can setup your sections and create passwords
    to create a password visit /results.php?keygen=<yourpassword> or /keys.php?keygen=<yourpassword>
  */

  // This is the master password: used for edit files; can also be used for upload everywhere. Keep it secret.
  $master_psw = '';

  $sects = array(
    array(
      'name' => 'Unofficial Sessions', // Section title: shown on top of the table
      'dir' => 'main/', // Directory of the section: this one is the default one
      'psw' => '', // A password specific to that section: useful for public upload
      'accept' => 'session_', // Prefix needed to upload the file in this section
      'limit' => 0, // Maximum number of files for this section
      'asc' => false // sorting: true A-Z, false Z-A
    ),
    array(
      'name' => 'Casual Events', // Section title: shown on top of the table
      'dir' => 'casual/', // Note the / at the end of the name, this should be kept
      'psw' => '', // A password specific to that section: useful for public upload
      'accept' => 'session_', // Prefix needed to upload the file in this section
      'limit' => 0, // No limit: this means no autodelete
      'asc' => false // sorting: true A-Z, false Z-A
    ),
    array(
      'name' => 'Competitive Events', // Section title: shown on top of the table
      'dir' => 'competitive/', // Note the / at the end of the name, this should be kept
      'psw' => '', // A password specific to that section: useful for public upload
      'accept' => 'session_', // Prefix needed to upload the file in this section
      'limit' => 0, // No limit: this means no autodelete
      'asc' => false // sorting: true A-Z, false Z-A
    ),
    array(
      'name' => 'Special Events', // Section title: shown on top of the table
      'dir' => 'special/', // Note the / at the end of the name, this should be kept
      'psw' => '', // A password specific to that section: useful for public upload
      'accept' => 'session_', // Prefix needed to upload the file in this section
      'limit' => 0, // No limit: this means no autodelete
      'asc' => false // sorting: true A-Z, false Z-A
    ),
    array(
      'name' => 'Tournaments', // Section title: shown on top of the table
      'dir' => 'tournament/', // Note the / at the end of the name, this should be kept
      'psw' => '', // A password specific to that section: useful for public upload
      'accept' => 'session_', // Prefix needed to upload the file in this section
      'limit' => 0, // No limit: this means no autodelete
      'asc' => false // sorting: true A-Z, false Z-A
    ),
  );

  if(isset($_GET['keygen'])) {
    echo password_hash($_GET['keygen'], PASSWORD_DEFAULT);
    die;
  }
?>
